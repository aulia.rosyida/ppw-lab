from django.shortcuts import render
from datetime import datetime, date

# Enter my name here
mhs_name = 'Aulia Rosyida' 
hobby = 'drawing'
curr_year = int(datetime.now().strftime("%Y"))
birth_date = date(1999, 11, 24)  
npm = '1706025346' 
college = 'University of Indonesia'
my_desc = "I'm undergraduate Student at Faculty of Computer Science"

right = 'Fakhira Devina'
left = 'Selvy Fitriani'

# Create my views here.
def index(request):
    response = {'name'    : mhs_name, 
				'age'     : calculate_age(birth_date.year), 
				'npm'     : npm, 
				'hobby'   : hobby,
				'college' : college,
				'desc'    : my_desc}
    return render(request, 'index_lab1.html', response)

def calculate_age(birth_year):
    return curr_year - birth_year if birth_year <= curr_year else 0
